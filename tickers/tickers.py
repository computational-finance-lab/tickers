"""
Utilities which return lists of stock symbols for several important indices.

TODO: Enable automatic updates of package resources
TODO: Handle symbol changes (merger, acquisition, de-listing, renaming, etc.)
"""
import argparse
import gzip
import json
import re
import ssl
from collections import defaultdict
from ftplib import FTP
from typing import Dict, List, Sequence, Tuple, Union
from urllib.request import Request, urlopen

import pandas as pd
import pkg_resources
from bs4 import BeautifulSoup

# This SSL context works around issues we were encountering on some systems.
# Warning: This allows for MitM attacks
# Reference: https://stackoverflow.com/questions/71603314/ssl-error-unsafe-legacy-renegotiation-disabled
ssl_conf = ssl.create_default_context()
ssl_conf.check_hostname = False
ssl_conf.verify_mode = ssl.CERT_NONE
ssl_conf.options |= 0x4


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--update",
        action="store_true",
        help="Trigger a local update to package resources. New content will be merged with old content by default.",
    )
    parser.add_argument(
        "--update_mode",
        choices={"merge", "overwrite"},
        default="merge",
        help="Controls the behavior for local updates to package resources.",
    )
    return parser


def symbol_lists_eq(a: Sequence[str], b: Sequence[str]) -> bool:
    a = set(a)
    b = set(b)
    out = a == b
    print(out)
    print(a - b)
    print(b - a)
    return out


def check_resource(r_name: str) -> bool:
    return pkg_resources.resource_exists(__name__, f"resources/{r_name}")


def get_resource_file(r_name: str) -> str:
    return pkg_resources.resource_filename(__name__, f"resources/{r_name}")


dow30_2016 = [
    "AAPL",
    "AXP",
    "BA",
    "CAT",
    "CSCO",
    "CVX",
    "DD",
    "DIS",
    "GE",
    "GS",
    "HD",
    "IBM",
    "INTC",
    "JNJ",
    "JPM",
    "KO",
    "MCD",
    "MMM",
    "MRK",
    "MSFT",
    "NKE",
    "PFE",
    "PG",
    "TRV",
    "UNH",
    "UTX",
    "V",
    "VZ",
    "WMT",
    "XOM",
]

snp30_2016 = [
    "AAPL",
    "AMZN",
    "BAC",
    "BRK.B",
    "CMCSA",
    "CVX",
    "DIS",
    "FB",
    "GE",
    "GOOG",
    "GOOGL",
    "HD",
    "IBM",
    "INTC",
    "JNJ",
    "JPM",
    "KO",
    "MRK",
    "MSFT",
    "ORCL",
    "PEP",
    "PFE",
    "PG",
    "PM",
    "T",
    "V",
    "VZ",
    "WFC",
    "WMT",
    "XOM",
]

combined_2016 = sorted(set(dow30_2016) | set(snp30_2016))

with open(get_resource_file("snp500_2016.csv")) as f:
    snp500_2016 = [x.strip().strip(",") for x in f]

with open(get_resource_file("russell_tickers.csv")) as f:
    r3k_super = [x.strip() for x in list(f.readlines())[129:]]


def get_dow30(by_tape: bool = False) -> Union[List[str], Tuple[List[str], List[str]]]:
    """
    Returns lists of stock symbols which were members of the Dow at any point between 2009 and 2016.

    Args:
        by_tape: Return a list of symbols for each tape, or a single list.

    Returns:
        Stock symbols in the Dow 30 as a single list or two lists split by membership to tape A/C.
    """
    tape_a = [
        "AA",
        "AXP",
        "BA",
        "BAC",
        "CAT",
        "CVX",
        "DD",
        "DIS",
        "GE",
        "GS",
        "HD",
        "HPQ",
        "IBM",
        "JNJ",
        "JPM",
        "KO",
        "MCD",
        "MMM",
        "MRK",
        "NKE",
        "PFE",
        "PG",
        "T",
        "TRV",
        "UNH",
        "UTX",
        "V",
        "VZ",
        "WMT",
        "XOM",
    ]

    tape_c = ["AAPL", "CSCO", "INTC", "KHC", "MSFT"]

    if by_tape:
        return tape_a, tape_c
    else:
        return sorted(tape_a + tape_c)


def get_snp500(start, end) -> List[str]:
    """
    Args:
        start: (date-like)
        end: (date-like)

    Returns:
        Symbols of stocks which were members of the S&P 500 at any point in [start_date, end_date].
    """
    start, end = pd.Timestamp(start).date(), pd.Timestamp(end).date()
    request = Request(
        "https://en.wikipedia.org/wiki/List_of_S%26P_500_companies",
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"
        },
    )
    soup = BeautifulSoup(urlopen(request, context=ssl_conf), "html.parser")
    base_table = soup.find("table", {"class": "wikitable sortable"}).findAll("tr")
    changes_table = soup.findAll("table", {"class": "wikitable sortable"})[1].findAll(
        "tr"
    )[2:]

    snp500 = []
    for row in base_table:
        try:
            symbol = row.findAll("td")[0].a.string
            if "/" in symbol:
                snp500.extend(symbol.split("/"))
            else:
                snp500.append(symbol)
        except IndexError:
            pass

    for row in changes_table:
        date = pd.to_datetime(row.findAll("td")[0].string).date()
        add_ticker = row.findAll("td")[1].string
        remove_ticker = row.findAll("td")[3].string

        if add_ticker:
            add_ticker = add_ticker.strip()
            if "/" in add_ticker:
                add_ticker = add_ticker.split("/")
        if remove_ticker:
            remove_ticker = remove_ticker.strip()
            if "/" in remove_ticker:
                remove_ticker = remove_ticker.split("/")

        if start <= date <= end:
            if add_ticker:
                if isinstance(add_ticker, list):
                    snp500.extend(add_ticker)
                else:
                    snp500.append(add_ticker)
            # Don't do anything with remove_symbol inside period of interest since we are
            # building a superset of members
        elif date > end:
            try:
                if isinstance(add_ticker, list):
                    snp500 = [x for x in snp500 if x not in add_ticker]
                else:
                    snp500.remove(add_ticker)
            except ValueError:
                pass
            if remove_ticker:
                if isinstance(remove_ticker, list):
                    snp500.extend(remove_ticker)
                else:
                    snp500.append(remove_ticker)
    return [str(x).strip() for x in sorted(set(snp500))]


def get_top_n_by_cap(count: int = 2000) -> List[str]:
    """
    Args:
        count: Number of stock symbols to return.

    Returns:
        Stock symbols ordered by market capitalization.
    """
    companies = pd.read_csv(get_resource_file("companylist.csv"), na_filter=False)
    companies.index = companies["Symbol"]
    companies = companies["MarketCap"]
    companies = companies.sort(
        ["MarketCap"], inplace=False, ascending=False, kind="mergesort"
    )
    return [str(x).strip() for x in list(companies.index[:count])]


def get_tickers_by_sector() -> Dict[str, List[str]]:
    """
    Returns:
        A mapping from sector to stock symbols that belong to that sector.
    """
    companies = pd.read_csv(get_resource_file("companylist.csv"), na_filter=False)
    sector2tickers = defaultdict(list)
    for company in companies.itertuples():
        sector2tickers[company.sector].append(company.symbol.strip())
    return sector2tickers


def get_tickers_by_class() -> Dict[str, List[str]]:
    """
    Returns:
        A mapping from common market capitalization classes to stock symbols that belong to that class.
    """
    companies = pd.read_csv(get_resource_file("companylist.csv"), na_filter=False)

    class2tickers = defaultdict(list)
    for company in companies.itertuples():
        if 2 * 10**11 <= company[3]:
            class2tickers["mega"].append(company[0])
        elif 10**10 <= company[3] < 2 * 10**11:
            class2tickers["large"].append(company[0])
        elif 2 * 10**9 <= company[3] < 10**10:
            class2tickers["mid"].append(company[0])
        elif 3 * 10**8 <= company[3] < 2 * 10**9:
            class2tickers["small"].append(company[0])
        elif 5 * 10**7 <= company[3] < 3 * 10**8:
            class2tickers["micro"].append(company[0])
        else:
            class2tickers["nano"].append(company[0])
    return class2tickers


def get_tapes(
    mode: str = "combined",
    drop_test_symbols: bool = True,
    drop_etfs: bool = False,
) -> Tuple[List[str], List[str], List[str]]:
    """
    Args:
        mode: Data source used to construct tapes. Choices = {'nyse', 'nasdaq', 'combined'}
        drop_test_symbols: Toggle the removal of test symbols
        drop_etfs: Toggle the removal of ETF symbols

    Returns:
        Tape A, B, and C, constructed using the union of symbols provided by NYSE and Nasdaq.
    """
    if mode == "nyse":
        tapes = get_tapes_nyse()
    elif mode == "nasdaq":
        tapes = get_tapes_nasdaq()
    elif mode == "combined":
        tapes = tuple(
            sorted(set(t1) | set(t2))
            for t1, t2 in zip(get_tapes_nasdaq(), get_tapes_nyse())
        )
    else:
        raise ValueError(
            f"Unexpected value for parameter mode = {mode}. "
            f"Should be one of {{'nasdaq', 'nyse', 'combined'}}"
        )

    if drop_test_symbols:
        tapes = tuple(
            sorted(set(full) - set(test))
            for full, test in zip(tapes, get_test_symbols())
        )

    if drop_etfs:
        tapes = tuple(
            sorted(set(full) - set(test))
            for full, test in zip(tapes, get_etf_symbols())
        )

    return tapes


def get_tapes_nasdaq() -> Tuple[List[str], List[str], List[str]]:
    """
    Returns:
        Tape A, B, and C, constructed using the symbols provided by Nasdaq.
    """
    nasdaq_symbols = pd.read_csv(
        get_resource_file("nasdaqlisted.txt"),
        delimiter="|",
        skipfooter=1,
        engine="python",
        na_filter=False,  # One of the trading symbols is NA, which is loaded as a NaN float value if na_filter=True
    )
    tape_c = sorted(nasdaq_symbols["Symbol"].astype(str).str.strip())

    other_symbols = pd.read_csv(
        get_resource_file("otherlisted.txt"),
        delimiter="|",
        skipfooter=1,
        engine="python",
        na_filter=False,  # One of the trading symbols is NA, which is loaded as a NaN float value if na_filter=True
    )
    tape_a = sorted(
        other_symbols.loc[other_symbols["Exchange"] == "N", "NASDAQ Symbol"].astype(str)
    )
    tape_b = sorted(
        other_symbols.loc[other_symbols["Exchange"] != "N", "NASDAQ Symbol"].astype(str)
    )

    return tape_a, tape_b, tape_c


def get_tapes_nyse() -> Tuple[List[str], List[str], List[str]]:
    """
    Returns:
        Tape A, B, and C, constructed using the symbols provided by NYSE.
    """
    nyse_symbols = pd.read_csv(
        get_resource_file("NYSESymbolMapping.txt"),
        delimiter="|",
        header=None,
        na_filter=False,  # One of the trading symbols is NA, which is loaded as a NaN float value if na_filter=True
    ).dropna(how="all", axis=1)
    arca_symbols = pd.read_csv(
        get_resource_file("ARCASymbolMapping.txt"),
        delimiter="|",
        header=None,
        na_filter=False,  # One of the trading symbols is NA, which is loaded as a NaN float value if na_filter=True
    ).dropna(how="all", axis=1)

    tape_a = sorted(
        set(nyse_symbols.loc[nyse_symbols[4] == "N", 1])
        | set(arca_symbols.loc[arca_symbols[4] == "N", 1])
    )
    tape_b = sorted(arca_symbols.loc[~arca_symbols[4].isin({"N", "Q"}), 1])
    tape_c = sorted(arca_symbols.loc[arca_symbols[4] == "Q", 1])
    return (
        translate_cqs_to_nasdaq(tape_a),
        translate_cqs_to_nasdaq(tape_b),
        translate_cqs_to_nasdaq(tape_c),
    )


def get_etf_symbols() -> Tuple[List[str], List[str], List[str]]:
    """
    Returns:
        ETF symbols split by tape, constructed using Nasdaq resources.
    """
    nasdaq_symbols = pd.read_csv(
        get_resource_file("nasdaqlisted.txt"),
        delimiter="|",
        skipfooter=1,
        engine="python",
    )
    tape_c = sorted(
        nasdaq_symbols.loc[nasdaq_symbols["ETF"] == "Y", "Symbol"].str.strip()
    )

    other_symbols = pd.read_csv(
        get_resource_file("otherlisted.txt"),
        delimiter="|",
        skipfooter=1,
        engine="python",
    )
    tape_a = sorted(
        other_symbols.loc[
            (other_symbols["ETF"] == "Y") & (other_symbols["Exchange"] == "N"),
            "NASDAQ Symbol",
        ]
    )
    tape_b = sorted(
        other_symbols.loc[
            (other_symbols["ETF"] == "Y") & (other_symbols["Exchange"] != "N"),
            "NASDAQ Symbol",
        ]
    )
    return tape_a, tape_b, tape_c


def get_test_symbols() -> Tuple[List[str], List[str], List[str]]:
    """
    Returns:
        Test symbols split by tape, constructed using Nasdaq resources.

    References:
        https://www.nyse.com/publicdocs/nyse/markets/nyse-american/NYSE_%20American_Pillar_Update_May_3_2017.pdf
        https://www.nyse.com/publicdocs/ctaplan/notifications/trader-update/CTS_CQS%20%20NEW%20DEDICATED%20TEST%20SYMBOL_09102015.pdf
    """
    nasdaq_symbols = pd.read_csv(
        get_resource_file("nasdaqlisted.txt"),
        delimiter="|",
        skipfooter=1,
        engine="python",
    )
    tape_c = sorted(
        nasdaq_symbols.loc[nasdaq_symbols["Test Issue"] == "Y", "Symbol"].str.strip()
    )

    other_symbols = pd.read_csv(
        get_resource_file("otherlisted.txt"),
        delimiter="|",
        skipfooter=1,
        engine="python",
    )
    tape_a = list(
        other_symbols.loc[
            (other_symbols["Test Issue"] == "Y") & (other_symbols["Exchange"] == "N"),
            "NASDAQ Symbol",
        ]
    ) + [
        "CBO-A",
        "CTEST.A",
        "MTEST",
        "MTEST.A",
        "NTEST.D",
        "NTEST.G",
        "NTEST.H",
        "NTEST.I",
        "NTEST.J",
        "NTEST.K",
        "NTEST.L",
        "NTEST.M",
        "NTEST.N",
        "NTEST.O",
        "NTEST.P",
        "NTEST.Q",
        "NTEST.Y",
        "NTEST.Z",
    ]
    tape_b = list(
        other_symbols.loc[
            (other_symbols["Test Issue"] == "Y") & (other_symbols["Exchange"] != "N"),
            "NASDAQ Symbol",
        ]
    ) + [
        "ATEST.G",
        "ATEST.H",
        "ATEST.L",
        "ATEST.Z",
        "M.TEST",
        "PTEST",
        "PTEST.A",
        "PTEST.B",
        "PTEST.W",
        "PTEST.X",
        "PTEST.Y",
        "PTEST.Z",
        "ZTST",
        "ZZK",
        "ZZZ",
    ]
    return sorted(tape_a), sorted(tape_b), sorted(tape_c)


def compare_nasdaq_nyse_tapes():
    labels = ["Tape A", "Tape B", "Tape C"]
    nasdaq_tapes = get_tapes_nasdaq()
    nyse_tapes = get_tapes_nyse()
    test_symbols = get_test_symbols()

    print("Investigating discrepancies between NYSE and Nasdaq symbol resources.")
    for label, t_nasdaq, t_nyse, t_test in zip(
        labels, nasdaq_tapes, nyse_tapes, test_symbols
    ):
        print(f"Comparing {label}")
        nasdaq_only = sorted((set(t_nasdaq) - set(t_nyse)) - set(t_test))
        print(f"Nasdaq only: {len(nasdaq_only)} -> {nasdaq_only}")
        nyse_only = sorted((set(t_nyse) - set(t_nasdaq)) - set(t_test))
        print(f"Nyse only: {len(nyse_only)} -> {nyse_only}\n")


def translate_cqs_to_nasdaq(symbols: List[str]) -> List[str]:
    """
    Args:
        symbols: Trading symbols to be translated

    Returns: Symbols translated from CQS to Nasdaq symbology

    References:
        https://nasdaqtrader.com/Trader.aspx?id=CQSSymbolConvention
    """
    pref_re = re.compile(r"(\w+)p(\w+)\b")
    out_symbols = []
    for symbol in symbols:
        symbol = (
            symbol.replace(".WS.A", "+A")
            .replace(".WS.B", "+B")
            .replace(".WS.C", "+C")
            .replace(".WS", "+")
            .replace(".WD", "$")
            .replace(".CL", "*")
            .replace(".U", "=")
            .replace("r", "^")
            .replace("w", "#")
            .replace("p", "-")
        )

        match = pref_re.match(symbol)
        if match:
            symbol = f"{match.group(1)}-{match.group(2)}"

        out_symbols.append(symbol)
    return out_symbols


def check_ticker_files() -> bool:
    """
    Returns: (bool)
        Indicates whether all required files are present.
    """
    return (
        check_resource("nasdaqlisted.txt")
        and check_resource("otherlisted.txt")
        and check_resource("NYSESymbolMapping.txt")
        and check_resource("ARCASymbolMapping.txt")
    )


def download_company_list():
    """
    References:
        https://stackoverflow.com/questions/69034318/downloading-all-listings-from-nasdaq-with-link-to-json-file
        https://stackoverflow.com/questions/61943209/accessing-nasdaq-historical-data-with-python-requests-results-in-connection-time/66334113#66334113
    """
    request = Request(
        "https://api.nasdaq.com/api/screener/stocks?"
        "table_only=true&region=North_America&country=United_States&download=true",
        headers={
            "Accept-Language": "en-US,en;q=0.9",
            "Accept-Encoding": "gzip, deflate, br",
            "User-Agent": "Java-http-client/",
        },
    )
    content = urlopen(request, context=ssl_conf).read()
    content = gzip.decompress(content)

    j = json.loads(content)
    # Returned JSON is a dict with keys = {"data", "message", "status"}
    # Under "data" is another dict with keys {"headers", "rows"}
    # "headers" is a dict from column labels to display names
    # "rows" is a list of dicts, each containing metadata for a single trading symbol
    table = j["data"]["rows"]
    df = pd.DataFrame(table)
    # TODO: Clean symbol names before output
    #     Seems to not follow CQS or Nasdaq symbology suffixes
    # df.symbol = df.symbol.str.replace("/", ".")
    df.to_csv(get_resource_file("companylist.csv"), index=False)


def download_ticker_files():
    # Get ticker files from NASDAQ
    ftp = FTP("ftp.nasdaqtrader.com")
    ftp.login()

    ftp.cwd("SymbolDirectory")
    ftp.retrbinary(
        "RETR nasdaqlisted.txt", open(get_resource_file("nasdaqlisted.txt"), "wb").write
    )
    ftp.retrbinary(
        "RETR otherlisted.txt", open(get_resource_file("otherlisted.txt"), "wb").write
    )

    ftp.quit()

    # Get ticker files from NYSE
    # TODO: Consider getting AmericanSymbolMapping, ChicagoSymbolMapping, and NationalSymbolMapping
    ftp = FTP("ftp.nyxdata.com")
    ftp.login()

    ftp.cwd("NYSESymbolMapping")
    ftp.retrbinary(
        f"RETR {ftp.nlst()[0]}",
        open(get_resource_file("NYSESymbolMapping.txt"), "wb").write,
    )

    ftp.cwd("../ARCASymbolMapping")
    ftp.retrbinary(
        f"RETR {ftp.nlst()[0]}",
        open(get_resource_file("ARCASymbolMapping.txt"), "wb").write,
    )
    ftp.quit()


def update_resources(update: bool = False, update_mode: str = "merge"):
    if not update:
        return

    if update_mode == "overwrite":
        download_ticker_files()
        download_company_list()
    elif update_mode == "merge":
        old_nasdaq_symbols = pd.read_csv(
            get_resource_file("nasdaqlisted.txt"),
            delimiter="|",
            skipfooter=1,
            engine="python",
            na_filter=False,
        )
        old_other_symbols = pd.read_csv(
            get_resource_file("otherlisted.txt"),
            delimiter="|",
            skipfooter=1,
            engine="python",
            na_filter=False,
        )
        old_nyse_symbols = pd.read_csv(
            get_resource_file("NYSESymbolMapping.txt"),
            delimiter="|",
            header=None,
            na_filter=False,
        ).dropna(how="all", axis=1)
        old_arca_symbols = pd.read_csv(
            get_resource_file("ARCASymbolMapping.txt"),
            delimiter="|",
            header=None,
            na_filter=False,
        ).dropna(how="all", axis=1)

        download_ticker_files()

        new_nasdaq_symbols = pd.read_csv(
            get_resource_file("nasdaqlisted.txt"),
            delimiter="|",
            skipfooter=1,
            engine="python",
            na_filter=False,
        )
        new_other_symbols = pd.read_csv(
            get_resource_file("otherlisted.txt"),
            delimiter="|",
            skipfooter=1,
            engine="python",
            na_filter=False,
        )
        new_nyse_symbols = pd.read_csv(
            get_resource_file("NYSESymbolMapping.txt"),
            delimiter="|",
            header=None,
            na_filter=False,
        ).dropna(how="all", axis=1)
        new_arca_symbols = pd.read_csv(
            get_resource_file("ARCASymbolMapping.txt"),
            delimiter="|",
            header=None,
            na_filter=False,
        ).dropna(how="all", axis=1)

        (
            pd.concat([old_nasdaq_symbols, new_nasdaq_symbols])
            .drop_duplicates("Symbol", keep="last")
            .sort_values("Symbol")
            .to_csv(get_resource_file("nasdaqlisted.txt"))
        )
        (
            pd.concat([old_other_symbols, new_other_symbols])
            .drop_duplicates("Symbol", keep="last")
            .sort_values("Symbol")
            .to_csv(get_resource_file("otherlisted.txt"))
        )
        (
            pd.concat([old_nyse_symbols, new_nyse_symbols])
            .drop_duplicates(1, keep="last")
            .sort_values(1)
            .to_csv(get_resource_file("NYSESymbolMapping.txt"))
        )
        (
            pd.concat([old_arca_symbols, new_arca_symbols])
            .drop_duplicates(1, keep="last")
            .sort_values(1)
            .to_csv(get_resource_file("ARCASymbolMapping.txt"))
        )

        # TODO: Ensure that this merge works as desired, there was a change in the table schema
        old_symbol_metadata = pd.read_csv(
            get_resource_file("companylist.csv"), na_filter=False
        )
        download_company_list()
        new_symbol_metadata = pd.read_csv(
            get_resource_file("companylist.csv"), na_filter=False
        )
        (
            pd.concat([old_symbol_metadata, new_symbol_metadata])
            .drop_duplicates("symbol", keep="last")
            .sort_values("symbol")
            .to_csv(get_resource_file("companylist.txt"))
        )
    else:
        raise ValueError(
            f"Unexpected value for update_mode = {update_mode}. Should be one of {{'merge', 'overwrite'}}."
        )


if not check_ticker_files():
    download_ticker_files()

if not check_resource("companylist.csv"):
    download_company_list()


if __name__ == "__main__":
    args = vars(get_parser().parse_args())
    update_resources(**args)

    tape_a, tape_b, tape_c = get_tapes()
    print(f"Tape A: {len(tape_a)}\nTape B: {len(tape_b)}\nTape C: {len(tape_c)}\n")

    compare_nasdaq_nyse_tapes()

    test_symbols = get_test_symbols()
    print(f"\nTape A Test Symbols: {len(test_symbols[0])} -> {test_symbols[0]}")
    print(f"Tape B Test Symbols: {len(test_symbols[1])} -> {test_symbols[1]}")
    print(f"Tape C Test Symbols: {len(test_symbols[2])} -> {test_symbols[2]}\n")

    with open(get_resource_file("russell_tickers.csv")) as f:
        russell_tickers = [x.strip() for x in f][129:]

    snp_total = get_snp500("2009/07/01", "2017/06/30")
    snp_2016 = get_snp500("2016/01/01", "2016/12/31")
    missing_snp = sorted(set(snp_total) - set(russell_tickers))
    print(
        f"S&P 2016 Count: {len(snp_2016)}\nS&P 2016 Missing: {sorted(set(snp_2016) - set(russell_tickers))}"
    )
    print(f"Missing S&P tickers: {missing_snp}\nMissing S&P Count: {len(missing_snp)}")

    sector2ticker = get_tickers_by_sector()
    sectors = sorted(sector2ticker.keys())
    if sectors[0] == "":
        sectors[0] = "N/A"
        sectors.sort()
    print(f"Sectors: {sectors}")

    missing_sectors = sorted(
        (set(russell_tickers) | set(snp_total))
        - set().union(
            *[set(vals) for key, vals in sector2ticker.items() if key != "N/A"]
        )
    )
    print(f"Tickers Missing Sectors: {missing_sectors}")
    print(
        f"\tCount: {len(missing_sectors)} / {len(set(russell_tickers) | set(snp_total))}"
    )
    print(f"Total S&P Count: {len(snp_total)}\nRussell Count: {len(russell_tickers)}")

    snp_2016_missing_sector = set(snp_2016) - set().union(
        *[set(vals) for key, vals in sector2ticker.items() if key != "N/A"]
    )
    print(
        f"2016 S&P Missing Sector:  ({len(snp_2016_missing_sector)}) {sorted(snp_2016_missing_sector)}"
    )

    snp_missing_sector = set(snp_total) - set().union(
        *[set(vals) for key, vals in sector2ticker.items() if key != "N/A"]
    )
    print(
        f"Total S&P Missing Sector: ({len(snp_missing_sector)}) {sorted(snp_missing_sector)}"
    )

    russell_missing_sector = set(russell_tickers) - set().union(
        *[set(vals) for key, vals in sector2ticker.items() if key != "N/A"]
    )
    print(
        f"Russell Missing Sector:   ({len(russell_missing_sector)}) {sorted(russell_missing_sector)}"
    )
