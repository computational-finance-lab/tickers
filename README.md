# tickers
## Utilities to facilitate working with tickers (stock symbols) and buckets of tickers.
## Getting started:
```bash
pip install git@gitlab.com:computational-finance-lab/tickers.git
```

 - You may want to create a python environment
   - Anaconda: `conda create -n compfi python=3.10 && source activate compfi`
